<?php get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>News</span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="news">
		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<article class="default">
					<?php the_content(); ?>
				</article>

			<?php endwhile; endif; ?>
	
		</div>
	</section>

<?php get_footer(); ?>