<?php get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('news_hero_image', 'options'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>News</span>
				</h2>
				<h1>
					<span><?php bloginfo('name'); ?></span>
				</h1>
			</div>

		</div>
	</section>


	<section id="news">
		<div class="wrapper">

			<?php

				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 8,
					'paged' => $paged,
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				  	<?php get_template_part('partials/news-article'); ?>

			<?php endwhile; endif; wp_reset_postdata(); ?>

			<?php if($query->max_num_pages > 1): ?>
				<?php echo do_shortcode('[ajax_load_more id="news" container_type="div" post_type="post" offset="8" pause="true" scroll="false" transition="fade" transition_container="false" button_label="More Posts"]'); ?>
			<?php endif; ?>
			
		</div>
	</section>

<?php get_footer(); ?>