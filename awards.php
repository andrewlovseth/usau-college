<?php

/*

	Template Name: Awards

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>

				<h3>Past Award Winners</h3>

				<?php if(have_rows('awards')): while(have_rows('awards')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'd_i' ): ?>
						
						<table>
							<thead>
							    <tr>
							        <td class="year">Year</td>
							        <td class="callahan">Callahan</td>
							        <td class="team-spirit">Team Spirt Award</td>
							    </tr>
							</thead>

							<tbody>
								<?php if(have_rows('d_i_awards')): while(have_rows('d_i_awards')): the_row(); ?>
								 
								    <tr>
								        <td class="year"><?php the_sub_field('year'); ?></td>
								        <td class="callahan"><?php the_sub_field('callahan'); ?></td>
								        <td class="team-spirit"><?php the_sub_field('team_spirit'); ?></td>
								    </tr>

								<?php endwhile; endif; ?>
							</tbody>					
						</table>

				    <?php endif; ?>

				    <?php if( get_row_layout() == 'd_iII' ): ?>
						
						<table>
							<thead>
							    <tr>
							        <td class="year">Year</td>
							        <td class="donovan">Donovan</td>
							        <td class="team-spirit">Team Spirt Award</td>
							    </tr>
							</thead>

							<tbody>
								<?php if(have_rows('d_iii_awards')): while(have_rows('d_iii_awards')): the_row(); ?>
								 
								    <tr>
								        <td class="year"><?php the_sub_field('year'); ?></td>
								        <td class="donovan"><?php the_sub_field('donovan'); ?></td>
								        <td class="team-spirit"><?php the_sub_field('team_spirit'); ?></td>
								    </tr>

								<?php endwhile; endif; ?>
							</tbody>					
						</table>

				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>

			</article>

		</div>
	</section>

<?php get_footer(); ?>