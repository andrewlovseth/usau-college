<?php

/*

	Template Name: Home

*/

get_header(); ?>


	<section id="hero">

		<?php if(have_rows('hero')): while(have_rows('hero')): the_row(); ?>
 
			<a href="<?php the_sub_field('link'); ?>" class="hero-link">

				<div class="hero-image" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
					<div class="wrapper">

						<div class="info">
							<h2>
								<span><?php the_sub_field('subheadline'); ?></span>
							</h2>
							<h1>
								<span><?php the_sub_field('headline'); ?></span>
							</h1>
						</div>


					</div>
				</div>

			</a>

		<?php endwhile; endif; ?>

	</section>


	<section id="main">
		<div class="wrapper">

			<section id="tiles">

				<?php if(have_rows('tiles')): $i = 1; while(have_rows('tiles')): the_row(); ?>

					<?php if($i == 1): ?><div class="col left"><?php endif; ?>
					<?php if($i == 4): ?></div><div class="col right"><?php endif; ?>

					 	<a class="cover tile tile-<?php echo $i; ?>"  href="<?php the_sub_field('link'); ?>" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
							<span class="label"><?php the_sub_field('label'); ?></span>
					    </a>

				    <?php if($i == 6): ?></div><?php endif; ?>

				<?php $i++; endwhile; endif; ?>

			</section>

			<aside id="latest-news">
				<h3>Latest News</h3>

				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 4
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php get_template_part('partials/quick-news-article'); ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>

				<a href="<?php echo site_url('/news/'); ?>" class="btn">All News</a>

			</aside>

		</div>
	</section>


<?php get_footer(); ?>