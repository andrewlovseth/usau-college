<?php

/*

	Template Name: Password Protected Photos

*/

get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $event = get_post_type(get_the_ID()); the_field($event . '_event_name', 'options'); ?></span>
				</h2>
				<h1>
					<span>Photos for Press</span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article>

				<div class="password-form">
					<?php the_content(); ?>
				</div>

				<?php if( !post_password_required( $post )): ?>

					<div class="description">
						<?php the_field('description'); ?>
					</div>

					<div class="photos">
						
						<?php if(have_rows('photos')): while(have_rows('photos')): the_row(); ?>
						 
						    <div class="photo">
						    	<a href="<?php $file = get_sub_field('file'); echo $file['url']?>">
						    		<img src="<?php $image = get_sub_field('thumbnail'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
						        	<span><?php the_sub_field('file_name'); ?></span>
						        </a>
						    </div>

						<?php endwhile; endif; ?>

					</div>

				<?php endif; ?>

			</article>

		</div>
	</section>

<?php get_footer(); ?>