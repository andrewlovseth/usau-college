<?php

/*

	Template Name: Road to Nationals

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">



			<section class="round" id="conferences">
				<div class="wrapper">

					<h2>
						Conferences.<br/>
						<span><?php the_field('number_of_conferences'); ?></span> Tournaments.<br/>
						<span><?php the_field('number_of_conference_teams'); ?></span> Teams.
					</h2>

					<div id="conferences-tab-links" class="tab-links">
						<?php if(have_rows('conferences')): while(have_rows('conferences')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'region' ): ?>							
						    	<a href="#conf-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a>
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>
					</div>

					<div id="conferences-tabs" class="tabs">
						<?php if(have_rows('conferences')): while(have_rows('conferences')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'region' ): ?>
								
								<div class="region" id="conf-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">
						    		<h3><?php the_sub_field('name'); ?></h3>
						    		<?php if(have_rows('conference')): while(have_rows('conference')): the_row(); ?>
									 
									    <div class="tournament">
									        <h4><?php the_sub_field('name'); ?></h4>
									        <h5><?php the_sub_field('date'); ?> - <?php the_sub_field('location'); ?></h5>

									        <p>
									        	<strong>Teams:</strong> <?php the_sub_field('number_of_teams'); ?><br/>
									        	<strong>Regional Qualifiers:</strong> <?php the_sub_field('number_of_qualifiers'); ?><br/>
									        	<strong>Champion:</strong> <?php the_sub_field('winner'); ?>
									        </p>

									        <a href="<?php the_sub_field('results_link'); ?>" rel="external">Full Results</a>
									    </div>

									<?php endwhile; endif; ?>
								</div>
								
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>
					</div>

				</div>
			</section>



			<section class="round" id="regionals">
				<div class="wrapper">

					<h2>
						Regionals.<br/>
						<span><?php the_field('number_of_regions'); ?></span> Tournaments.<br/>
						<span><?php the_field('number_of_regionals_team'); ?></span> Teams.
					</h2>

					<div id="regionals-tab-links" class="tab-links">
						<?php if(have_rows('regionals')): while(have_rows('regionals')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'region' ): ?>							
						    	<a href="#conf-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a>
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>
					</div>

					<div id="regionals-tabs" class="tabs">
						<?php if(have_rows('regionals')): while(have_rows('regionals')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'region' ): ?>
								
								<div class="region" id="conf-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">

									<div class="tournament">
						    			<h3><?php the_sub_field('name'); ?></h3>							 
								        <h5><?php the_sub_field('date'); ?> - <?php the_sub_field('location'); ?></h5>

								        <p>
								        	<strong>Teams:</strong> <?php the_sub_field('number_of_teams'); ?><br/>
								        	<strong>Champion:</strong> <?php the_sub_field('winner'); ?>
								        	<?php if(get_sub_field('qualifiers')): ?>
									        	<br/><strong>Qualifiers:</strong> <?php the_sub_field('qualifiers'); ?>
									        <?php endif; ?>

								        </p>

								        <a href="<?php the_sub_field('results_link'); ?>" rel="external">Full Results</a>
								    </div>
								</div>
								
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>
					</div>

				</div>
			</section>


			<section class="round" id="nationals">
				<div class="wrapper">

					<h2>
						Nationals.<br/>
						<span><?php the_field('number_of_nationals_teams'); ?></span> Teams.<br/>
						<span>1</span> Champion.
					</h2>

					<div class="tournament">
		    			<h3><?php the_field('nationals_name'); ?></h3>							 
				        <h5><?php the_field('nationals_date'); ?> - <?php the_field('nationals_location'); ?></h5>

				        <p>
				        	<a href="<?php the_field('nationals_schedule_link'); ?>">Schedule &amp; Results</a>
				        	<a href="<?php the_field('nationals_teams_link'); ?>">Teams</a>
				        </p>

				    </div>

			   </div>
			</section>


		</div>
	</section>

<?php get_footer(); ?>