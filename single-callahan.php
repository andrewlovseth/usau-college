<?php get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php the_field('school'); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<div class="back">
				<?php
					if(get_field('division') == 'men') {
						$backURL = '/d1-men/callahan/';
					}
					if(get_field('division') == 'women') {
						$backURL = '/d1-women/callahan/';
					}
				?>

				<a href="<?php echo site_url($backURL); ?>">&lt; Back to all nominees</a>
			</div>

			<article>

				<div id="bio">
					<h4>Biography</h4>

					<img src="<?php $image = get_field('headshot'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

					<?php if(get_field('bio')): ?>
						<?php the_field('bio'); ?>
					<?php endif; ?>

					<?php if(get_field('video_link')): ?>
						<a href="<?php the_field('video_link'); ?>" class="btn" rel="external">View Nominee Video</a>
					<?php endif; ?>

				</div>

			</article>

			<div class="aside-wrapper">

				<aside id="vitals">
					<h3>Vitals</h3>

					<h5>Name</h5>
					<p><?php the_title(); ?></p>


					<?php if(get_field('school')): ?>
						<h5>School</h5>
						<p><?php the_field('school'); ?></p>
					<?php endif; ?>

					<?php if(get_field('team_name')): ?>
						<h5>Team Name</h5>
						<p><?php the_field('team_name'); ?></p>
					<?php endif; ?>

					<?php if(get_field('year')): ?>
						<h5>Year</h5>
						<p><?php the_field('year'); ?></p>
					<?php endif; ?>

					<?php if(get_field('hometown')): ?>
						<h5>Hometown</h5>
						<p><?php the_field('hometown'); ?></p>
					<?php endif; ?>

				</aside>

			</div>

		</div>
	</section>
	
	<?php get_template_part('partials/callahan-partners'); ?>

<?php get_footer(); ?>