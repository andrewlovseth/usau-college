<?php get_header(); ?>


	<section id="video-player">
		<div class="wrapper">

			<div id="video-content">

				<div class="player">

		        	<?php if(have_rows('video_id')): while(have_rows('video_id')) : the_row(); ?>

					    <?php if( get_row_layout() == 'youtube' ): ?>
				        	<iframe src="http://www.youtube-nocookie.com/embed/<?php the_sub_field('id'); ?>?rel=0&showinfo=0&modestbranding=1&vq=hd1080&autoplay=0" frameborder="0" width="1920" height="1080" allowfullscreen></iframe>
					    <?php endif; ?>

					    <?php if( get_row_layout() == 'vimeo' ): ?>
					    	<iframe src="http://player.vimeo.com/video/<?php the_sub_field('id'); ?>?title=0&color=a30034&autoplay=1&byline=0" frameborder="0" width="1920" height="1080"></iframe>      
					    <?php endif; ?>
					 
					<?php endwhile; endif; ?>

				</div>
	    
	    		<div class="info">
		   	    	<h3><?php the_title(); ?></h3>
	        		<?php the_field('description'); ?>
	        	</div>

	        </div>
	
		</div>
	</section>

<?php get_footer(); ?>