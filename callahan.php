<?php

/*

	Template Name: Callahan

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<section id="overview">
				<div class="overview-wrapper">
					<?php the_field('overview'); ?>
				</div>

				<div class="vote">
					<?php if(have_rows('voting_buttons')): while(have_rows('voting_buttons')): the_row(); ?>
					 
					    <div class="button">
					        <a href="<?php the_sub_field('link'); ?>" class="btn" rel="external"><?php the_sub_field('label'); ?></a>
					    </div>

					<?php endwhile; endif; ?>
				</div>

				<div class="tabs">
					<a href="#nominees">Nominees</a>
					<a href="#rules">Rules</a>
				</div>

			</section>

			<article>
				<section id="nominees">

					<div class="header">
						<h2>Nominees</h2>
					</div>


					<?php
						$division = get_field('division');
						$args = array(
							'post_type' => 'callahan',
							'order' => 'ASC',
							'orderby' => 'title',
							'posts_per_page' => 100,
							'meta_key' => 'division',
							'meta_value' => $division,
							'meta_compare' => '='
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

							<div class="nominee">
								<div class="headshot">
									<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('headshot'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
								</div>

								<div class="info">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?><em><?php the_field('school'); ?></em></a></h3>
								</div>
							</div>

					<?php endwhile; endif; wp_reset_postdata(); ?>

				</section>

				<section id="rules">
					<h2>Rules</h2>
					<?php the_field('rules'); ?>
				</section>

			</article>

		</div>
	</section>

	<?php get_template_part('partials/callahan-partners'); ?>

<?php get_footer(); ?>