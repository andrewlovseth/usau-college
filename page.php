<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section id="generic-page">
			<div class="wrapper">
				
				<article class="default">

					<div class="article-header">
						<h1><?php the_title(); ?></h1>
					</div>

					<div class="article-body">
						<?php the_content(); ?>
					</div>

				</article>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>