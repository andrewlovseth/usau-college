<div class="result">
	<div class="time">
		<?php if(get_sub_field('time') !== ''): ?>
			<?php the_sub_field('time'); ?>
		<?php endif; ?>
	</div>

	<div class="match-up">
		<span class="usa">USA</span>
		<span class="versus"><?php the_sub_field('usa_score'); ?> - <?php the_sub_field('opponent_score'); ?></span>
		<span class="opponent"><?php the_sub_field('opponent'); ?></span>
    </div>

	<div class="link">
		<?php if(get_sub_field('link') !== ''): ?>
			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_label'); ?></a>
		<?php endif; ?>
	</div>
</div>