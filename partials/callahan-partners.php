<?php if(have_rows('callahan_partners', 'options')): ?>
	
	<section id="callahan-partners">
		<div class="wrapper">

			<h2><?php the_field('callahan_partners_headline', 'options'); ?></h2>

			<div id="sponsors-wrapper">

				<?php while(have_rows('callahan_partners', 'options')): the_row(); ?>

					<?php $image = get_sub_field('logo'); ?>

					<a href="<?php the_sub_field('link'); ?>" rel="external" class="<?php echo $image['alt']; ?>">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>

				<?php endwhile; ?>

			</div>

		</div>

	</section>
<?php endif; ?>