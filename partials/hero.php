<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	<div class="wrapper">

		<div class="info">
			<h2>
				<span><?php $parentID = wp_get_post_parent_id($post->ID); the_field('event_name', $parentID); ?></span>
			</h2>
			<h1>
				<span><?php the_title(); ?></span>
			</h1>
		</div>

	</div>
</section>