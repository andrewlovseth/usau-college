<?php

/*

	Template Name: News

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>


	<section id="news">
		<div class="wrapper">

			<?php

				$categories = '';
				$terms = get_field('categories');
				if( $terms ) {
					foreach( $terms as $term ) {
						$categories .= $term->slug . ', ';
					}
				}

				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'post',
					'category_name' => $categories,
					'posts_per_page' => 4,
					'paged' => $paged,
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				  	<?php get_template_part('partials/news-article'); ?>

			<?php endwhile; endif; wp_reset_postdata(); ?>

			<?php if($query->max_num_pages > 1): ?>
				<?php echo do_shortcode('[ajax_load_more id="news" category="' . $categories . '" container_type="div" post_type="post" offset="4" pause="true" scroll="false" transition="fade" transition_container="false" button_label="More Posts"]'); ?>
			<?php endif; ?>
			
		</div>
	</section>

<?php get_footer(); ?>