<?php get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php the_field('full_school_name'); ?></span>
				</h2>
				<h1>
					<span><?php the_field('team_name'); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article>

				<div id="bio">
					<h4>Team Biography</h4>

					<img src="<?php $image = get_field('logo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

					<?php if(get_field('bio')): ?>
						<?php the_field('bio'); ?>
					<?php endif; ?>
				</div>

				<div id="team-history">

					<?php if(get_field('city')): ?>
						<h5>City</h5>
						<p><?php the_field('city'); ?></p>
					<?php endif; ?>

					<?php if(get_field('conference')): ?>
						<h5>Conference</h5>
						<p><?php the_field('conference'); ?></p>
					<?php endif; ?>

					<?php if(get_field('region')): ?>
						<h5>Region</h5>
						<p><?php the_field('region'); ?></p>
					<?php endif; ?>

					<?php if(get_field('year_founded')): ?>
						<h5>Year Founded</h5>
						<p><?php the_field('year_founded'); ?></p>
					<?php endif; ?>

					<?php if(get_field('college_championship_appearences')): ?>
						<h5>College Championship Appearances</h5>
						<p><?php the_field('college_championship_appearences'); ?></p>
					<?php endif; ?>

					<?php if(get_field('best_finish')): ?>
						<h5>Best Finish</h5>
						<p><?php the_field('best_finish'); ?></p>
					<?php endif; ?>

					<?php if(get_field('coaches')): ?>
						<h5>Coaches</h5>
						<p><?php the_field('coaches'); ?></p>
					<?php endif; ?>

				</div>

			</article>

			<div class="aside-wrapper">

				<aside id="roster">
					<h3>Roster</h3>
					<?php if(have_rows('roster')): while(have_rows('roster')): the_row(); ?>
					 
					    <div class="player">
					    	<span class="number"><?php the_sub_field('number'); ?></span>
					    	<span class="name"><?php the_sub_field('name'); ?></span>
					    	<span class="year"><?php the_sub_field('year'); ?></span>
					    </div>

					<?php endwhile; endif; ?>



				</aside>

			</div>

		</div>
	</section>

<?php get_footer(); ?>