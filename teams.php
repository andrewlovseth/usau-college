<?php

/*

	Template Name: Teams

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<section id="teams-wrapper">


			<?php
				$teamCat = get_field('category', $post->ID);

				$args = array(
					'post_type' => 'team',
					'posts_per_page' => 30,
					'cat' => $teamCat,
					'order' => 'asc',
					'orderby' => 'title'
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="team">
					<div class="logo">
						<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('logo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
					</div>

					<div class="info">
						<h3><a href="<?php the_permalink(); ?>"><?php the_field('school'); ?></a></h3>
					</div>


				</div>

			<?php endwhile; endif; wp_reset_postdata(); ?>


			</section>

		</div>
	</section>

<?php get_footer(); ?>