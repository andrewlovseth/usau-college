<?php

/*

	Template Name: Watch Live

*/

get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>College Championships</span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article class="default">
				<?php the_field('description'); ?>
			</article>


			<?php if(have_rows('d_i_schedule')): ?>
				<section class="schedule">

					<h3>D-I Championships Streaming Schedule</h3>
	
					<?php while(have_rows('d_i_schedule')) : the_row(); ?>
				 
					    <?php if( get_row_layout() == 'day' ): ?>
							
							<div class="day">
					    		<h4><?php the_sub_field('day_title'); ?></h4>

					    		<table>
					    			<thead>
					    				<tr>
					    					<th class="round">Round</th>
					    					<th class="match-up">Match-up</th>
					    					<th class="time">Time (EDT)</th>
					    					<th class="link">Watch</th>
					    				</tr>
					    			</thead>

					    			<tbody>	
	
						    			<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
						    				<tr>
						    					<td class="round"><?php the_sub_field('round'); ?></td>
						    					<td class="match-up"><?php the_sub_field('match_up'); ?></td>
						    					<td class="time"><?php the_sub_field('time'); ?></td>
						    					<?php if(get_sub_field('link')): ?>
							    					<td class="link"><a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('provider'); ?></a></td>
							    				<?php else: ?>
							    					<td class="link"><?php the_sub_field('provider'); ?></td>
								    			<?php endif; ?>
						    				</tr>
						    			<?php endwhile; endif; ?>

					    			</tbody>
					    		</table>
							</div>
							
					    <?php endif; ?>
				 
					<?php endwhile; ?>

				</section>
			<?php endif; ?>


			<?php if(have_rows('d_iii_schedule')): ?>
				<section class="schedule">

					<h3>D-III Championships Streaming Schedule</h3>
	
					<?php while(have_rows('d_iii_schedule')) : the_row(); ?>
				 
					    <?php if( get_row_layout() == 'day' ): ?>
							
							<div class="day">
					    		<h4><?php the_sub_field('day_title'); ?></h4>

					    		<table>
					    			<thead>
					    				<tr>
					    					<th class="round">Round</th>
					    					<th class="match-up">Match-up</th>
					    					<th class="time">Time (EDT)</th>
					    					<th class="link">Watch</th>
					    				</tr>
					    			</thead>

					    			<tbody>	
	
						    			<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
						    				<tr>
						    					<td class="round"><?php the_sub_field('round'); ?></td>
						    					<td class="match-up"><?php the_sub_field('match_up'); ?></td>
						    					<td class="time"><?php the_sub_field('time'); ?></td>
						    					<?php if(get_sub_field('link')): ?>
							    					<td class="link"><a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('provider'); ?></a></td>
							    				<?php else: ?>
							    					<td class="link"><?php the_sub_field('provider'); ?></td>
								    			<?php endif; ?>
						    				</tr>
						    			<?php endwhile; endif; ?>

					    			</tbody>
					    		</table>
							</div>
							
					    <?php endif; ?>
				 
					<?php endwhile; ?>

				</section>
			<?php endif; ?>

		</div>
	</section>

<?php get_footer(); ?>