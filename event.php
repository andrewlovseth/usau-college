<?php

/*

	Template Name: Event

*/

get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php the_field('event_name'); ?></span>
				</h2>
				<h1>
					<span><?php the_field('year'); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<section id="overview">

				<div class="details">
					<div class="detail">
						<h5>Location</h5>
						<p><?php the_field('location'); ?></p>
					</div>

					<div class="detail">
						<h5>Date</h5>
						<p><?php the_field('date'); ?></p>
					</div>

					<div class="detail">
						<h5>Winner</h5>
						<p><?php the_field('winner'); ?></p>
					</div>

				</div>

				<div class="description">
					<?php the_field('overview'); ?>
				</div>
			</section>

			<section id="results">
				<div class="standings">
					<h3>Final Standings</h3>
					<?php the_field('standings'); ?>
				</div>

				<div class="spirit">
					<h3>Final Spirit Standings</h3>
					<?php the_field('spirit_scores'); ?>
				</div>

				<div class="results-link">
					<a href="<?php the_field('results_link'); ?>" class="btn" rel="external">Full Results</a>
				</div>
			</section>

			<?php if(have_rows('videos')): ?>

				<section id="videos">

					<h3>Videos</h3>

					<div class="video-wrapper">

						<?php while(have_rows('videos')): the_row(); ?>
					 
						    <div class="video">
						    	<div class="thumbnail">
						    		<a href="<?php the_sub_field('link'); ?>" rel="external">
						    			<img src="<?php $image = get_sub_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    		</a>
						    	</div>

						    	<div class="info">
						    		<h4><a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('title'); ?></a></h4>
						    	</div>
						       
						    </div>

						<?php endwhile; ?>

					</div>

				</section>

			<?php endif; ?>

		</div>
	</section>

<?php get_footer(); ?>